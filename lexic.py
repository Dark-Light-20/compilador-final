from colorama import init
from colorama import Fore, Back, Style

def lexicAnalysis(sequence):
    init()
    #types=['INT','STR','BOOL']
    #reserved=['START','END','IF','ELSE','ENDIF','WHILE','ENDWHILE','PRINT','READ']
    operators=['+','-','*','/','^','(',')','&','|','<','>','!','=']
    symbols=['{','}',';',',']
    lexicSequence=[]
    initPositions=[]
    available=False     # available -> to push an 'I'
    available2=False    # validate: (a '=' already analyzed after other operator) or (double logic operator)
    count=0             # number of "continue"

    for pos, char in enumerate(sequence):   # create a tuple (pos, char) of sequence
        if count!=0:
            count-=1
        elif char.isnumeric():
            available=True
        elif char in operators:
            if available:
                lexicSequence.append("I")
                initPositions.append(pos)
                available=False
            if available2:
                available2=False
                continue
            if char == '<':
                if pos < len(sequence)-1 and sequence[pos+1] == '=':
                    lexicSequence.append("<=")
                    initPositions.append(pos)
                    available2=True
                    continue
            elif char == '>':
                if pos < len(sequence)-1 and sequence[pos+1] == '=':
                    lexicSequence.append(">=")
                    initPositions.append(pos)
                    available2=True
                    continue
            elif char == '!':
                if pos < len(sequence)-1 and sequence[pos+1] == '=':
                    lexicSequence.append("!=")
                    initPositions.append(pos)
                    available2=True
                    continue
            elif char == '=':
                if pos < len(sequence)-1 and sequence[pos+1] == '=':
                    lexicSequence.append("==")
                    initPositions.append(pos)
                    available2=True
                    continue 
            elif char == '|':
                if pos < len(sequence)-1 and sequence[pos+1] == '|':
                    lexicSequence.append("||")
                    initPositions.append(pos)
                    available2=True
                    continue
                else:
                    return error(pos, char, sequence)     
            elif char == '&':
                if pos < len(sequence)-1 and sequence[pos+1] == '&':
                    lexicSequence.append("&&")
                    initPositions.append(pos)
                    available2=True
                    continue
                else:
                    return error(pos, char, sequence)        
            
            lexicSequence.append(char)
            initPositions.append(pos)
        
        elif char in symbols:
            if available:
                lexicSequence.append("I")
                initPositions.append(pos)
                available=False
            lexicSequence.append(char)
            initPositions.append(pos)

        elif char == '"':                   # For " strings "
            n=pos+1
            while sequence[n] != '"':
                n+=1
                count+=1
            #string=sequence[pos+1:n]       # For traduction xD
            count+=1
            lexicSequence.append('IDTEXT')
            initPositions.append(pos)

        elif char.isspace():                # for space chars (' ', '\t', '\n')
            if available:
                lexicSequence.append("I")
                initPositions.append(pos)
                available=False
            pass
        
        else:
            if char == 'S' and pos+4 < len(sequence) and sequence[pos:pos+5] == 'START':
                word='START'
                count=4
            elif char == 'E' and pos+7 < len(sequence) and sequence[pos:pos+8] == 'ENDWHILE':
                word='ENDWHILE'
                count=7
            elif char == 'E' and pos+4 < len(sequence) and sequence[pos:pos+5] == 'ENDIF':
                word='ENDIF'
                count=4
            elif char == 'E' and pos+3 < len(sequence) and sequence[pos:pos+4] == 'ELSE':
                word='ELSE'
                count=3
            elif char == 'I' and pos+1 < len(sequence) and sequence[pos:pos+2] == 'IF':
                word='IF'
                count=1
            elif char == 'W' and pos+4 < len(sequence) and sequence[pos:pos+5] == 'WHILE':
                word='WHILE'
                count=4
            elif char == 'P' and pos+4 < len(sequence) and sequence[pos:pos+5] == 'PRINT':
                word='PRINT'
                count=4
            elif char == 'R' and pos+3 < len(sequence) and sequence[pos:pos+4] == 'READ':
                word='READ'
                count=3
            elif char == 'E' and pos+2 < len(sequence) and sequence[pos:pos+3] == 'END':
                word='END'
                count=2
            elif char == 'I' and pos+2 < len(sequence) and sequence[pos:pos+3] == 'INT':
                word='INT'
                count=2
            elif char == 'S' and pos+2 < len(sequence) and sequence[pos:pos+3] == 'STR':
                word='STR'
                count=2
            elif char == 'B' and pos+3 < len(sequence) and sequence[pos:pos+4] == 'BOOL':
                word='BOOL'
                count=3
            else:
                if char.isalpha() and not available:
                    n=pos+1
                    while pos < len(sequence)-1 and sequence[n].isalnum():
                        n+=1
                        count+=1
                    word="IDVAR"
                else:
                    return error(pos, char, sequence)
            
            if available:
                lexicSequence.append("I")
                initPositions.append(pos)
                available=False
            lexicSequence.append(word)
            initPositions.append(pos)

    if available:                           # validation if last element in sequence is 'I'
        lexicSequence.append("I")
        initPositions.append(pos)
    
    lexicSequence.append("$")               # end of sequence char
    initPositions.append(pos)
    
    return True, lexicSequence, initPositions

def error(pos, char, sequence):
    print("Lexic Error!!! Extrange Character found at "+str(pos)+": "+char, '\n')
    
    string1 = sequence[:pos]
    string2 = sequence[pos+1:]

    print(string1+Fore.RED+char+Fore.WHITE+string2)
       
    return False, char, pos