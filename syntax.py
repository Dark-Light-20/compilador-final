from colorama import init
from colorama import Fore, Back, Style

class syntaxAnalysis():
    def __init__(self):
        self.stack = []
        self.stack.append("S")
        self.i=0
        init()

    def loop(self, sequence):
        while(self.i<len(sequence)):
            element = sequence[self.i]

            if len(self.stack) > 0:
                top=self.stack[-1]
            else:
                top = ''

            if element == 'I':
                if top == "P":
                    self.stack.pop()
                elif top == "F":
                    self.six()
                elif top == "T":
                    self.four()
                elif top == "E":
                    self.one()
                elif top == "ER":
                    self.thirteen()
                elif top == "EL1":
                    self.eleven()
                elif top == "OL":
                    self.nine()
                elif top == "ASG":
                    self.p_eleven()
                else:
                    break
            elif element == '+':
                if top == "F_L":
                    self.three()
                elif top == "T_L":
                    self.three()
                elif top == "E_L":
                    self.two()
                else:
                    break
            elif element == '-':
                if top == "F_L":
                    self.three()
                elif top == "T_L":
                    self.three()
                elif top == "E_L":
                    self.two()
                else:
                    break
            elif element == '*':
                if top == "F_L":
                    self.three()
                elif top == "T_L":
                    self.five()
                else:
                    break
            elif element == '/':
                if top == "F_L":
                    self.three()
                elif top == "T_L":
                    self.five()
                else:
                    break
            elif element == '^':
                if top == "F_L":
                    self.seven()
                else:
                    break
            elif element == '(':
                if top == "P":
                    self.eight()
                elif top == "F":
                    self.six()
                elif top == "T":
                    self.four()
                elif top == "E":
                    self.one()
                elif top == "ER":
                    self.thirteen()
                elif top == "EL1":
                    self.eleven()
                elif top == "OL":
                    self.nine()
                elif top == "ASG":
                    self.p_eleven()
                elif top == "(":
                    self.stack.pop()
                else:
                    break
            elif element == ')':
                if top == ")":
                    self.stack.pop()
                elif top == "F_L":
                    self.three()
                elif top == "T_L":
                    self.three()
                elif top == "E_L":
                    self.three()
                elif top == "ER_L":
                    self.three()
                elif top == "OL2_L":
                    self.three()
                elif top == "OL_L":
                    self.three()
                else:
                    break
            elif element == '&&':
                if top == "F_L":
                    self.three()
                elif top == "T_L":
                    self.three()
                elif top == "E_L":
                    self.three()
                elif top == "ER_L":
                    self.three()
                elif top == "OL2_L":
                    self.twelve()
                else:
                    break
            elif element == '||':
                if top == "F_L":
                    self.three()
                elif top == "T_L":
                    self.three()
                elif top == "E_L":
                    self.three()
                elif top == "ER_L":
                    self.three()
                elif top == "OL2_L":
                    self.three()
                elif top == "OL_L":
                    self.ten()
                else:
                    break
            elif element == '<':
                if top == "F_L":
                    self.three()
                elif top == "T_L":
                    self.three()
                elif top == "E_L":
                    self.three()
                elif top == "OR":
                    self.stack.pop()
                elif top == "ER_L":
                    self.fourteen()
                else:
                    break
            elif element == '>':
                if top == "F_L":
                    self.three()
                elif top == "T_L":
                    self.three()
                elif top == "E_L":
                    self.three()
                elif top == "OR":
                    self.stack.pop()
                elif top == "ER_L":
                    self.fourteen()
                else:
                    break
            elif element == '<=':
                if top == "F_L":
                    self.three()
                elif top == "T_L":
                    self.three()
                elif top == "E_L":
                    self.three()
                elif top == "OR":
                    self.stack.pop()
                elif top == "ER_L":
                    self.fourteen()
                else:
                    break
            elif element == '>=':
                if top == "F_L":
                    self.three()
                elif top == "T_L":
                    self.three()
                elif top == "E_L":
                    self.three()
                elif top == "OR":
                    self.stack.pop()
                elif top == "ER_L":
                    self.fourteen()
                else:
                    break
            elif element == '!=':
                if top == "F_L":
                    self.three()
                elif top == "T_L":
                    self.three()
                elif top == "E_L":
                    self.three()
                elif top == "OR":
                    self.stack.pop()
                elif top == "ER_L":
                    self.fourteen()
                else:
                    break
            elif element == '==':
                if top == "F_L":
                    self.three()
                elif top == "T_L":
                    self.three()
                elif top == "E_L":
                    self.three()
                elif top == "OR":
                    self.stack.pop()
                elif top == "ER_L":
                    self.fourteen()
                else:
                    break
            elif element == '$':
                if top == "F_L":
                    self.three()
                elif top == "T_L":
                    self.three()
                elif top == "E_L":
                    self.three()
                elif top == "ER_L":
                    self.three()
                elif top == "OL2_L":
                    self.three()
                elif top == "OL_L":
                    self.three()
                elif top == "INST":
                    self.three()
                elif top == '':
                    return "Sequence Accepted!!!", self.i
                else:
                    break
            ############## Program Syntactic
            elif element == 'START':
                if top == "S":
                    self.p_one()
                else:
                    break
            elif element == 'END':
                if top == "INST":
                    self.three()
                elif top == "END":
                    self.stack.pop()
                else:
                    break
            elif element == 'IF':
                if top == "INST":
                    self.p_four()
                else:
                    break
            elif element == 'ELSE':
                if top == "E_IF":
                    self.p_seven()
                else:
                    break
            elif element == 'ENDIF':
                if top == "E_IF":
                    self.stack.pop()
                elif top == "ENDIF":
                    self.stack.pop()
                else:
                    break
            elif element == 'WHILE':
                if top == "INST":
                    self.p_five()
                else:
                    break
            elif element == 'ENDWHILE':
                if top == "ENDWHILE":
                    self.stack.pop()
                else:
                    break
            elif element == 'PRINT':
                if top == "INST":
                    self.p_six()
                else:
                    break
            elif element == 'READ':
                if top == "ASG":
                    self.p_thirteen()
                else:
                    break
            elif element == '{':
                if top == "{":
                    self.stack.pop()
                else:
                    break
            elif element == '}':
                if top == "}":
                    self.stack.pop()
                elif top == "INST":
                    self.three()
                else:
                    break
            elif element == '=':
                if top == "=":
                    self.stack.pop()
                elif top == "DEC_L":
                    self.p_ten()
                else:
                    break
            elif element == ';':
                if top == ";":
                    self.stack.pop()
                elif top == "F_L":
                    self.three()
                elif top == "T_L":
                    self.three()
                elif top == "E_L":
                    self.three()
                elif top == "ER_L":
                    self.three()
                elif top == "OL2_L":
                    self.three()
                elif top == "OL_L":
                    self.three()
                elif top == "DEC_C":
                    self.three()
                elif top == "DEC_L":
                    self.three()
                else:
                    break
            elif element == ',':
                if top == "DEC_C":
                    self.p_nine()
                elif top == "F_L":
                    self.three()
                elif top == "T_L":
                    self.three()
                elif top == "E_L":
                    self.three()
                elif top == "ER_L":
                    self.three()
                elif top == "OL2_L":
                    self.three()
                elif top == "OL_L":
                    self.three()
                elif top == "DEC_L":
                    self.three()
                else:
                    break
            elif element in ('INT','STR','BOOL'):
                if top == "TYPE":
                    self.stack.pop()
                elif top == "INST":
                    self.p_two()
                else:
                    break
            elif element == 'IDTEXT':
                if top == "OUT":
                    self.stack.pop()
                elif top == "ASG":
                    self.stack.pop()
                else:
                    break
            elif element == 'IDVAR':
                if top == "P":
                    self.stack.pop()
                elif top == "F":
                    self.six()
                elif top == "T":
                    self.four()
                elif top == "E":
                    self.one()
                elif top == "ER":
                    self.thirteen()
                elif top == "EL1":
                    self.eleven()
                elif top == "OL":
                    self.nine()
                elif top == "OUT":
                    self.stack.pop()
                elif top == "ASG":
                    self.p_eleven()
                elif top == "INST":
                    self.p_three()
                elif top == "DEC":
                    self.p_eight()
                else:
                    break
            
            self.i+=1
        
        return self.error(top, element)
            
    def one(self):
        self.stack.pop()
        self.stack.append("E_L")
        self.stack.append("T")
        self.i-=1

    def two(self):
        self.stack.pop()
        self.stack.append("E_L")
        self.stack.append("T")
    
    def three(self):
        self.stack.pop()
        self.i-=1

    def four(self):
        self.stack.pop()
        self.stack.append("T_L")
        self.stack.append("F")
        self.i-=1

    def five(self):
        self.stack.pop()
        self.stack.append("T_L")
        self.stack.append("F")
    
    def six(self):
        self.stack.pop()
        self.stack.append("F_L")
        self.stack.append("P")
        self.i-=1
        
    def seven(self):
        self.stack.pop()
        self.stack.append("F_L")
        self.stack.append("P")
    
    def eight(self):
        self.stack.pop()
        self.stack.append(")")
        self.stack.append("OL")

    def nine(self):
        self.stack.pop()
        self.stack.append("OL_L")
        self.stack.append("EL1")
        self.i-=1
    
    def ten(self):
        self.stack.pop()
        self.stack.append("OL_L")
        self.stack.append("EL1")
    
    def eleven(self):
        self.stack.pop()
        self.stack.append("OL2_L")
        self.stack.append("ER")
        self.i-=1
    
    def twelve(self):
        self.stack.pop()
        self.stack.append("OL2_L")
        self.stack.append("ER")

    def thirteen(self):
        self.stack.pop()
        self.stack.append("ER_L")
        self.stack.append("E")
        self.i-=1
    
    def fourteen(self):
        self.stack.pop()
        self.stack.append("E")
        self.stack.append("OR")
        self.i-=1

    #### Program funcs

    def p_one(self):
        self.stack.pop()
        self.stack.append("END")
        self.stack.append("INST")

    def p_two(self):
        self.stack.pop()
        self.stack.append("INST")
        self.stack.append(";")
        self.stack.append("DEC")
        self.stack.append("TYPE")
        self.i-=1

    def p_three(self):
        self.stack.pop()
        self.stack.append("INST")
        self.stack.append(";")
        self.stack.append("ASG")
        self.stack.append("=")
        
    def p_four(self):
        self.stack.pop()
        self.stack.append("INST")
        self.stack.append("E_IF")
        self.stack.append("}")
        self.stack.append("INST")
        self.stack.append("{")
        self.stack.append(")")
        self.stack.append("OL")
        self.stack.append("(")

    def p_five(self):
        self.stack.pop()
        self.stack.append("INST")
        self.stack.append("ENDWHILE")
        self.stack.append("}")
        self.stack.append("INST")
        self.stack.append("{")
        self.stack.append(")")
        self.stack.append("OL")
        self.stack.append("(")

    def p_six(self):
        self.stack.pop()
        self.stack.append("INST")
        self.stack.append(";")
        self.stack.append(")")
        self.stack.append("OUT")
        self.stack.append("(")

    def p_seven(self):
        self.stack.pop()
        self.stack.append("ENDIF")
        self.stack.append("}")
        self.stack.append("INST")
        self.stack.append("{")
    
    def p_eight(self):
        self.stack.pop()
        self.stack.append("DEC_C")
        self.stack.append("DEC_L")

    def p_nine(self):
        self.stack.pop()
        self.stack.append("DEC")

    def p_ten(self):
        self.stack.pop()
        self.stack.append("ASG")

    def p_eleven(self):
        self.stack.pop()
        self.stack.append("OL")
        self.i-=1

    def p_thirteen(self):
        self.stack.pop()
        self.stack.append(")")
        self.stack.append("(")

    ##### Errors

    def error(self, top, element):
        if top == 'ENDIF':
            strError = "Syntax Error!!! 'ENDIF' expected but ' "+element+" ' arrived at position:"+str(self.i)

        elif top == 'ENDWHILE':
            strError = "Syntax Error!!! 'ENDWHILE' expected but ' "+element+" ' arrived at position:"+str(self.i)

        elif top == ';':
            strError = "Syntax Error!!! ';' expected but ' "+element+" ' arrived at position:"+str(self.i)

        elif top == '=':
            strError = "Syntax Error!!! '=' expected but ' "+element+" ' arrived at position:"+str(self.i)

        elif top == '(':
            strError = "Syntax Error!!! '(' expected but ' "+element+" ' arrived at position:"+str(self.i)

        elif top == ')':
            strError = "Syntax Error!!! ')' expected but ' "+element+" ' arrived at position:"+str(self.i)

        elif top == '{':
            strError = "Syntax Error!!! '{' expected but ' "+element+" ' arrived at position:"+str(self.i)

        elif top == '}':
            strError = "Syntax Error!!! '}' expected but ' "+element+" ' arrived at position:"+str(self.i)

        elif top == 'P':
            strError = "Syntax Error!!! '(', Identifier (Numeric value) or Variable expected but ' "+element+" ' arrived at position:"+str(self.i)

        elif top == 'F_L':
            strError = "Syntax Error!!! characters [,-)-;], operators (+,-,*,/,^,&&,||,<,>,=) or end of string expected but ' "+element+" ' arrived at position:"+str(self.i)

        elif top == 'F':
            strError = "Syntax Error!!! '(', Identifier (Numeric value) or Variable expected but ' "+element+" ' arrived at position:"+str(self.i)

        elif top == 'T_L':
            strError = "Syntax Error!!! characters [,-)-;], operators (+,-,*,/,&&,||,<,>,=) or end of string expected but ' "+element+" ' arrived at position:"+str(self.i)

        elif top == 'T':
            strError = "Syntax Error!!! '(', Identifier (Numeric value) or Variable expected but ' "+element+" ' arrived at position:"+str(self.i)

        elif top == 'E_L':
            strError = "Syntax Error!!! characters [,-)-;], operators (+,-,&&,||,<,>,=) or end of string expected but ' "+element+" ' arrived at position:"+str(self.i)

        elif top == 'E':
            strError = "Syntax Error!!! '(', Identifier (Numeric value) or Variable expected but ' "+element+" ' arrived at position:"+str(self.i)

        elif top == 'OR':
            strError = "Syntax Error!!! operators (<,>,=)  expected but ' "+element+" ' arrived at position:"+str(self.i)

        elif top == 'ER_L':
            strError = "Syntax Error!!! characters [,-)-;], operators (&&,||,<,>,=) or end of string expected but ' "+element+" ' arrived at position:"+str(self.i)

        elif top == 'ER':
            strError = "Syntax Error!!! '(', Identifier (Numeric value) or Variable expected but ' "+element+" ' arrived at position:"+str(self.i)

        elif top == 'OL2_L':
            strError = "Syntax Error!!! characters [,-)-;], operators (&&,||) or end of string expected but ' "+element+" ' arrived at position:"+str(self.i)

        elif top == 'EL1':
            strError = "Syntax Error!!! '(', Identifier or Variable (Numeric value) expected but ' "+element+" ' arrived at position:"+str(self.i)

        elif top == 'OL_L':
            strError = "Syntax Error!!! characters [,-)-;], operator '||' or end of string expected but ' "+element+" ' arrived at position:"+str(self.i)

        elif top == 'OL':
            strError = "Syntax Error!!! '(', Identifier (Numeric value) or Variable expected but ' "+element+" ' arrived at position:"+str(self.i)

        elif top == 'OUT':
            strError = "Syntax Error!!! Variable (Numeric value or Text) expected but ' "+element+" ' arrived at position:"+str(self.i)

        elif top == 'E_IF':
            strError = "Syntax Error!!! 'ELSE' or 'ENDIF' expected but ' "+element+" ' arrived at position:"+str(self.i)

        elif top == 'ASG':
            strError = "Syntax Error!!! '(', 'READ' or Variable (Numeric value or Text) expected but ' "+element+" ' arrived at position:"+str(self.i)

        elif top == 'DEC_C':
            strError = "Syntax Error!!! characters [,-;] expected but ' "+element+" ' arrived at position:"+str(self.i)

        elif top == 'DEC_L':
            strError = "Syntax Error!!! characters [=-,-;] expected but ' "+element+" ' arrived at position:"+str(self.i)

        elif top == 'DEC':
            strError = "Syntax Error!!! Variable expected but ' "+element+" ' arrived at position:"+str(self.i)

        elif top == 'TYPE':
            strError = "Syntax Error!!! Variable declaration (INT,STR,BOOLEAN) expected but ' "+element+" ' arrived at position:"+str(self.i)

        elif top == 'INST':
            strError = "Syntax Error!!! '}', Instructions (END,IF,WHILE,PRINT), Variable declaration (INT,STR,BOOLEAN), Variable, Identifier (Numeric value) or end of string expected but ' "+element+" ' arrived at position:"+str(self.i)

        elif top == 'S':
            strError = "Syntax Error!!! 'START' expected but ' "+element+" ' arrived at position:"+str(self.i)

        elif top == 'END':
            strError = "Syntax Error!!! 'END' expected but ' "+element+" ' arrived at position:"+str(self.i)

        elif top == '':
            strError = "Syntax Error!!! end of string expected but ' "+element+" ' arrived at position:"+str(self.i)

        #return "Syntax Error!!! It was expected ' "+top+" ' and it arrived ' "+element+" ', at position: "+str(self.i)

        """return strError"""

        return strError, self.i   # For syntax error location

    def errorLocation(self, location, expression, positions, txt_output, redColor, greenColor):
        string1 = expression[:positions[location]]

        if location == len(positions)-1:
            error = expression[positions[location]:]
            print(string1+Fore.RED+error+Fore.WHITE)
            txt_output.insertPlainText(string1)
            txt_output.setTextColor(redColor)
            txt_output.insertPlainText(error)
            txt_output.setTextColor(greenColor)
        else:
            error = expression[positions[location]:positions[location+1]]
            string2 = expression[positions[location+1]:]
            
            print(string1+Fore.RED+error+Fore.WHITE+string2)
            txt_output.insertPlainText(string1)
            txt_output.setTextColor(redColor)
            txt_output.insertPlainText(error)
            txt_output.setTextColor(greenColor)
            txt_output.insertPlainText(string2)