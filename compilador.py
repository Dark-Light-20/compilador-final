# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'compilador.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

"""
Nota:

Tener instalado:

    - python 3
    - pip
    - PyQt5
    - colorama
"""

"""
Presentado por:

Sebastian Alexander Diaz Paz
Peter D'Loise Chicaiza Cortez
Juan Esteban Castro Guerrero
Juan Sebastian Cardona Narvaez
Sergio Gomez Duque
"""

from PyQt5 import QtCore, QtGui, QtWidgets
from lexic import lexicAnalysis
from syntax import syntaxAnalysis
import sys
import resources

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(373, 329)
        Dialog.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:1 rgba(23, 96, 135, 255));")
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(100, 10, 171, 61))
        self.label.setStyleSheet("font: 14pt \"Segoe UI\";")
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(Dialog)
        self.label_2.setGeometry(QtCore.QRect(100, 70, 175, 17))
        self.label_2.setStyleSheet("font: 10pt \"Segoe UI\";")
        self.label_2.setAlignment(QtCore.Qt.AlignCenter)
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(Dialog)
        self.label_3.setGeometry(QtCore.QRect(100, 130, 171, 17))
        self.label_3.setStyleSheet("font: 10pt \"Segoe UI\";")
        self.label_3.setAlignment(QtCore.Qt.AlignCenter)
        self.label_3.setObjectName("label_3")
        self.label_4 = QtWidgets.QLabel(Dialog)
        self.label_4.setGeometry(QtCore.QRect(120, 110, 124, 17))
        self.label_4.setStyleSheet("font: 10pt \"Segoe UI\";")
        self.label_4.setAlignment(QtCore.Qt.AlignCenter)
        self.label_4.setObjectName("label_4")
        self.label_5 = QtWidgets.QLabel(Dialog)
        self.label_5.setGeometry(QtCore.QRect(90, 90, 192, 17))
        self.label_5.setStyleSheet("font: 10pt \"Segoe UI\";")
        self.label_5.setAlignment(QtCore.Qt.AlignCenter)
        self.label_5.setObjectName("label_5")
        self.label_6 = QtWidgets.QLabel(Dialog)
        self.label_6.setGeometry(QtCore.QRect(120, 150, 125, 17))
        self.label_6.setStyleSheet("font: 10pt \"Segoe UI\";")
        self.label_6.setAlignment(QtCore.Qt.AlignCenter)
        self.label_6.setObjectName("label_6")
        self.label_8 = QtWidgets.QLabel(Dialog)
        self.label_8.setGeometry(QtCore.QRect(110, 190, 151, 131))
        self.label_8.setMinimumSize(QtCore.QSize(151, 0))
        self.label_8.setText("")
        self.label_8.setPixmap(QtGui.QPixmap(":/udenar-logo/udenar.png"))
        self.label_8.setScaledContents(True)
        self.label_8.setWordWrap(False)
        self.label_8.setObjectName("label_8")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.label.setText(_translate("Dialog", "Creado por:"))
        self.label_2.setText(_translate("Dialog", "Juan Esteban Castro Guerrero"))
        self.label_3.setText(_translate("Dialog", "Sebastian Alexander Diaz Paz"))
        self.label_4.setText(_translate("Dialog", "Peter D\'loise Chicaiza"))
        self.label_5.setText(_translate("Dialog", "Juan Sebastián Cardona Narvaez"))
        self.label_6.setText(_translate("Dialog", "Sergio Gómez Duque"))

class Ui_Compilador(object):
    def compilar(self):
        redColor = QtGui.QColor(255, 0, 0)
        greenColor = QtGui.QColor(36, 199, 20)
        expression = self.txt_input.toPlainText()
        #expression="START\n\tINT var1 = 3, var2 var;\n\tIF(54+4&&52>=8){\n\t\tSTR var3 = READ();\n\t}ENDIF\nEND"
        
        if not expression:
            self.txt_output.clear()
            self.txt_output.insertPlainText("Empty program...")   
        else:
            print(expression)
            print("----------------")
            results=lexicAnalysis(expression)

            if results[0]:                  ### Lexic verification
                #lexicSequence=' '.join(map(str, results[1]))
                lexicSequence=results[1].__repr__()
                print(lexicSequence)
                self.txt_output.clear()
                self.txt_output.insertPlainText(lexicSequence+"\n\n")
                print("----------------")
                analysis=syntaxAnalysis()
                final=analysis.loop(results[1])
                print(final[0], '\n')
                self.txt_output.insertPlainText(final[0]+"\n\n")
                if final[0] != "Sequence Accepted!!!":      ### Syntax verification
                    location=final[1]
                    positions=results[2]
                    analysis.errorLocation(location, expression, positions, self.txt_output, redColor, greenColor)
                    print("----------------")
            else:
                char = results[1]
                pos = results[2]
                sequence= expression
                self.txt_output.clear()
                self.txt_output.insertPlainText("Lexic Error!!! Extrange Character found at "+ str(pos)+ ": "+char+"\n\n")
                self.txt_output.setTextColor(greenColor)
                self.txt_output.insertPlainText(str(sequence[:pos]))
                self.txt_output.setTextColor(redColor)
                self.txt_output.insertPlainText(char)
                self.txt_output.setTextColor(greenColor)
                self.txt_output.insertPlainText(str(sequence[pos+1:]))
            
    def dialog(self):
        Dialog = QtWidgets.QDialog()
        ui = Ui_Dialog()
        ui.setupUi(Dialog)
        Dialog.exec_()
               
    def setupUi(self, Compilador):
        Compilador.setObjectName("Compilador")
        Compilador.resize(809, 573)
        Compilador.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:1 rgba(23, 96, 135, 255));")
        self.centralwidget = QtWidgets.QWidget(Compilador)
        self.centralwidget.setObjectName("centralwidget")
        self.txt_input = QtWidgets.QTextEdit(self.centralwidget)
        self.txt_input.setGeometry(QtCore.QRect(30, 50, 351, 401))
        self.txt_input.setTabStopWidth(20)
        self.txt_input.setStyleSheet("font: 8pt \"Segoe UI\";\n"
                                     "background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:1 rgba(172, 189, 186, 255));")
        self.txt_input.setObjectName("txt_input")
        self.txt_output = QtWidgets.QTextEdit(self.centralwidget)
        self.txt_output.setGeometry(QtCore.QRect(400, 50, 371, 401))
        self.txt_output.setStyleSheet("font: 8pt \"Segoe UI\";\n"
                                      #"color: green"";\n"
                                      "\n""background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(19, 46, 50, 255), stop:1 rgba(19, 46, 50, 255));")
        self.txt_output.setTextColor(QtGui.QColor(36, 199, 20))
        self.txt_output.setObjectName("txt_output")
        self.txt_output.setReadOnly(True)
        self.txt_output.setTabStopWidth(20)
        self.txt_output.setText("")
        self.btn_compilar = QtWidgets.QPushButton(self.centralwidget)
        self.btn_compilar.setGeometry(QtCore.QRect(150, 480, 141, 41))
        self.btn_compilar.setStyleSheet("font: 8pt \"Segoe UI\";\n""background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:1 rgb(0, 159, 183));")
        self.btn_compilar.setObjectName("btn_compilar")
        self.btn_compilar.clicked.connect(lambda:self.compilar())
        self.btn_about = QtWidgets.QPushButton(self.centralwidget)
        self.btn_about.clicked.connect(lambda:self.dialog())
        self.btn_about.setGeometry(QtCore.QRect(730, 0, 81, 31))
        self.btn_about.setStyleSheet("font: 8pt \"Segoe UI\";")
        self.btn_about.setObjectName("btn_about")
        Compilador.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(Compilador)
        self.statusbar.setObjectName("statusbar")
        Compilador.setStatusBar(self.statusbar)

        self.retranslateUi(Compilador)
        QtCore.QMetaObject.connectSlotsByName(Compilador)

    def retranslateUi(self, Compilador):
        _translate = QtCore.QCoreApplication.translate
        Compilador.setWindowTitle(_translate("Compilador", "MainWindow"))
        self.txt_output.setToolTip(_translate("Compilador", "<html><head/><body><p><span style=\" color:#00ff00;\"></span></p></body></html>"))
        self.txt_output.setWhatsThis(_translate("Compilador", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
                                                "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
                                                "p, li { white-space: pre-wrap; }\n"
                                                "</style></head><body style=\" font-family:\'Segoe UI\'; font-size:8pt; font-weight:400; font-style:normal;\">\n"
                                                "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>"))
        self.btn_compilar.setText(_translate("Compilador", "Compilar"))
        self.btn_about.setText(_translate("Compilador", "About"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Compilador = QtWidgets.QMainWindow()
    ui = Ui_Compilador()
    ui.setupUi(Compilador)
    Compilador.show()
    sys.exit(app.exec_())